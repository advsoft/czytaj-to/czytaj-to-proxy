/**
 * Created by johniak on 22.02.17.
 */
var host = '0.0.0.0';
var port = 8888;

var cors_proxy = require('cors-anywhere');
cors_proxy.createServer({
    originWhitelist: [], // Allow all origins
    removeHeaders: ['cookie', 'X-Frame-Options', 'x-frame-options']
}).listen(port, host, function() {
    console.log('Running CORS Anywhere on ' + host + ':' + port);
});